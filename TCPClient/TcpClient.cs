﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using KirasaModelsDBLib;

namespace TCPClient
{

    public class TcpClient
    {
        #region Variables
        int LEN_ARRAY = 1000;
        private System.Net.Sockets.TcpClient tcpClient;
        string typespath = Path.Combine(Directory.GetCurrentDirectory(), @"ProductTypes.txt");
        IDictionary<string, string> types = new Dictionary<string, string>();
        #endregion
        #region Threads
        private Thread thrRead;
        public NetworkStream streamClient;
        #endregion
        #region Events
        public event EventHandler OnConnect;
        public event EventHandler OnDisconnect;

        public event EventHandler<string> OnReadByte;

        public event EventHandler<MyEventArgsAeroScopeData> OnUpdateAeroscopeData;

        #endregion

        public TcpClient()
        {
            try
            {
                types = File
                        .ReadLines(typespath)
                        .Select((v, i) => new { Index = i, Value = v })
                        .GroupBy(p => p.Index / 2)
                        .ToDictionary(g => g.First().Value, g => g.Last().Value);
            }
            catch (Exception ex)
            {

            }
        }

        int connectionWasSet = 0;
        /// <summary>
        /// connect to server
        /// </summary>
        /// <param name="strIPClient"></param>
        /// <param name="iPortClient"></param>
        /// <param name="strIPServer"></param>
        /// <param name="iPortServer"></param>
        /// <returns></returns>
        public bool Connect(string strIPClient, int iPortClient, string strIPServer, int iPortServer)
        {
            try
            {
                // create client
                tcpClient = new System.Net.Sockets.TcpClient();

            
                // begin connect 
                var result = tcpClient.BeginConnect(strIPServer, iPortServer, null, null);

                // try connect
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(5));

                if (!success)
                {
                    throw new Exception("Failed to connect.");
                }

                // we have connected
                tcpClient.EndConnect(result);
            }
            catch (System.Exception ex)
            {
                //generate event 
                OnDisconnect?.Invoke(this, EventArgs.Empty);
                return false;
            }

            if (tcpClient.Connected)
            {
                try
                {
                    // create stream for client
                    streamClient = tcpClient.GetStream();
                    // create thread for reading
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                 catch (Exception ex)
                {
                    // generate event
                    OnDisconnect?.Invoke(this, EventArgs.Empty);
                    return false;
                }
            }

            if (connectionWasSet == 0) { OnConnect?.Invoke(this, EventArgs.Empty); }//invoked just one time. When first connection established. For NOT writing to Console in every connection case;
            connectionWasSet = 1;
            return true;
        }

        /// <summary>
        /// Connects to AeroScope (no thread reading variant)
        /// </summary>
        /// <param name="strIPClient"></param>
        /// <param name="iPortClient"></param>
        /// <param name="strIPServer"></param>
        /// <param name="iPortServer"></param>
        /// <returns></returns>
        public bool ConnectNoReadThr(string strIPClient, int iPortClient, string strIPServer, int iPortServer)
        {
            try
            {              
                // create client
                tcpClient = new System.Net.Sockets.TcpClient();

                // begin connect 
                var result = tcpClient.BeginConnect(strIPServer, iPortServer, null, null);

                // try connect
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(5));

                if (!success)
                {
                    throw new Exception("Failed to connect.");
                }

                // we have connected
                tcpClient.EndConnect(result);
            }
            catch (System.Exception ex)
            {
                //generate event 
                OnDisconnect?.Invoke(this, EventArgs.Empty);
                return false;
            }

            if (tcpClient.Connected)
            {
                try
                {
                    streamClient = tcpClient.GetStream();
                    //no read thread. Jusr waiting aeroscope data in readData function.
                    ReadDataNoReadThr();                   
                }
                catch (Exception ex)
                {
                    // generate event
                    OnDisconnect?.Invoke(this, EventArgs.Empty);
                    return false;
                }
            }
            if (connectionWasSet == 0) { OnConnect?.Invoke(this, EventArgs.Empty); }//invoked just one time. When first connection established. For NOT writing to Console in every connection case;
            connectionWasSet = 1;
            return true;
        }
        public void Disconnect()
        {
            DisconnectTcp();
        }

        // disconnet from server
        private void DisconnectTcp()
        {
            // if there is client
            if (streamClient != null)
            {
                // close client stream
                try
                {
                    streamClient.Close();
                }
                catch (System.Exception)
                {

                }
            }
            // if there is client
            if (tcpClient != null)
            {
                // close client connection
                try
                {
                    tcpClient.Close();
                    tcpClient = null;
                }
                catch (System.Exception)
                {

                }
            }
            // generate event
            OnDisconnect?.Invoke(this, EventArgs.Empty);
        }

        public bool _continue = false;
        Tuple<TableAeroscope, TableAeroscopeTrajectory> result;
        public string strPackage { get; private set; }
        /// <summary>
        ///  Read data (byte) (thread)
        /// </summary>
        public void ReadData()
        {
           
            byte[] bBufRead = new byte[LEN_ARRAY];
            byte[] bBufSave = new byte[LEN_ARRAY];

            int iReadByte = -1;
            while (_continue)
            {
                try
                {
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    iReadByte = streamClient.Read(bBufRead, 0, bBufRead.Length); 
                   

                    if (iReadByte > 0)
                    {
                        if (OnReadByte != null)
                            OnReadByte(this, strPackage);

                        Array.Resize(ref bBufRead, iReadByte);

                        //if (iReadByte == 97 || iReadByte==100 || iReadByte == 124 || iReadByte==163 ||iReadByte==109  )
                        if (iReadByte == 97)
                        {
                            try
                            {
                                strPackage = BitConverter.ToString(bBufRead);
                                strPackage = strPackage.Replace("-", string.Empty);
                                result = this.Parse1(strPackage);
                                TableAeroscope aerodata = result.Item1;
                                TableAeroscopeTrajectory aeroscopeTrajectory = result.Item2;
                                if (string.IsNullOrEmpty(aerodata.Type)) { aerodata.Type = string.Empty; }
                                if (!string.IsNullOrEmpty(aerodata.SerialNumber) && aerodata.SerialNumber.Length > 4)
                                {
                                    OnUpdateAeroscopeData?.Invoke(this, new MyEventArgsAeroScopeData(aerodata, aeroscopeTrajectory));                            
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        if (iReadByte == 109)
                        {
                            try
                            {
                                strPackage = BitConverter.ToString(bBufRead);
                                strPackage = strPackage.Replace("-", string.Empty);
                                strPackage = strPackage + "";
                                result = this.Parse2(strPackage);
                                TableAeroscope aerodata = result.Item1;
                                TableAeroscopeTrajectory aeroscopeTrajectory = result.Item2;

                                if (string.IsNullOrEmpty(aerodata.Type)) { aerodata.Type = string.Empty; }
                                if (!string.IsNullOrEmpty(aerodata.SerialNumber) && aerodata.SerialNumber.Length>4)
                                { 
                                  OnUpdateAeroscopeData?.Invoke(this, new MyEventArgsAeroScopeData(aerodata, aeroscopeTrajectory));                               
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                   
                }

                catch (System.Exception ex)
                {

                }
            }
        }

        /// <summary>
        /// Waits read and parse bytes from AeroScope 
        /// </summary>
        //int exitcounter = 0;
        public void ReadDataNoReadThr()
        {
            //exitcounter = 0;
            _continue = true;//added 03122020 for M2 version
            byte[] bBufRead = new byte[LEN_ARRAY];
            byte[] bBufSave = new byte[LEN_ARRAY];

            int iReadByte = -1;
            while (_continue)
            {
                try
                {
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    iReadByte =  streamClient.Read(bBufRead, 0, bBufRead.Length);
                    //Console.WriteLine(iReadByte);
                    //exitcounter += 1; 
                    if (iReadByte >= 97)
                    {
                        if (OnReadByte != null)
                            OnReadByte(this, strPackage);

                        Array.Resize(ref bBufRead, iReadByte);

                        //if (iReadByte == 97 || iReadByte==100 || iReadByte == 124 || iReadByte==163 ||iReadByte==109  )
                        if (iReadByte == 97)
                        {
                            try
                            {
                                strPackage = BitConverter.ToString(bBufRead);
                                strPackage = strPackage.Replace("-", string.Empty);
                                result = this.Parse1(strPackage);
                                TableAeroscope aerodata = result.Item1;
                                TableAeroscopeTrajectory aeroscopeTrajectory = result.Item2;
                                if (string.IsNullOrEmpty(aerodata.Type)) { aerodata.Type = string.Empty; }

                                //if (aerodata.SerialNumber.Length > 0 && aerodata.Type.Length != 0 && aerodata.UUIDLength == aerodata.UUID.Length)
                                if (!string.IsNullOrEmpty(aerodata.SerialNumber) && aerodata.SerialNumber.Length > 4)
                                {
                                    OnUpdateAeroscopeData?.Invoke(this, new MyEventArgsAeroScopeData(aerodata, aeroscopeTrajectory));
                                    _continue = false;//added 03122020 for M2 version ; commented 08122020 should work faster not interrupting while not all packets have been read
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        if (iReadByte == 109)
                        {
                            try
                            {
                                strPackage = BitConverter.ToString(bBufRead);
                                strPackage = strPackage.Replace("-", string.Empty);
                                strPackage = strPackage + "";
                                result = this.Parse2(strPackage);
                                TableAeroscope aerodata = result.Item1;
                                TableAeroscopeTrajectory aeroscopeTrajectory = result.Item2;

                                if (string.IsNullOrEmpty(aerodata.Type)) { aerodata.Type = string.Empty; }
                                if (!string.IsNullOrEmpty(aerodata.SerialNumber) && aerodata.SerialNumber.Length > 4)
                                {
                                    OnUpdateAeroscopeData?.Invoke(this, new MyEventArgsAeroScopeData(aerodata, aeroscopeTrajectory));
                                    _continue = false;//added 03122020 for M2 version ; commented 08122020 should work faster not interrupting while not all packets have been read
                                }
                            }
                            catch (Exception ex)
                            {
                              
                            }
                        }
                        //if(exitcounter>=10)
                        //{
                        //    _continue = false;
                        //}
                    }

                }

                catch (System.Exception ex)
                {
                    _continue = false;               
                }
            }
        }

        /// <summary>
        /// Parsing of data packets  from emulator
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private Tuple<TableAeroscope, TableAeroscopeTrajectory> Parse1(string str)
        {
            TableAeroscope aeroscopedata = new TableAeroscope();
            TableAeroscopeTrajectory aeroscopetrajectory = new TableAeroscopeTrajectory();
            try
            {
                if (str != null)
                {
                    aeroscopedata.SerialNumber = string.Empty;
                    aeroscopedata.UUID = string.Empty;
                    string hexSerialNumber = str.Substring(50, 32);///правильно ли так считать serial number?

                    for (int i = hexSerialNumber.Length - 2; i >= 0; i -= 2)
                    {
                        if (hexSerialNumber[i] == '0' && hexSerialNumber[i + 1] == '0')
                            hexSerialNumber = hexSerialNumber.Remove(i);
                        else break;
                    }
                    string hexLng = string.Concat(str[88], str[89], str[86], str[87], str[84], str[85], str[82], str[83]);
                    string hexLat = string.Concat(str[96], str[97], str[94], str[95], str[92], str[93], str[90], str[91]);
                    string hexHomeLng = string.Concat(str[136], str[137], str[134], str[135], str[132], str[133], str[130], str[131]);
                    string hexHomeLat = string.Concat(str[144], str[145], str[142], str[143], str[140], str[141], str[138], str[139]);
                    string hexHeight = string.Concat(str[104], str[105], str[102], str[103]);//str.Substring(102, 2);
                    string hexProductType = str.Substring(146, 2);
                    string hexYaw = string.Concat(str[128], str[129], str[126], str[127]);// str.Substring(126, 2);
                    string hexRoll = string.Concat(str[124], str[125], str[122], str[123]);// str.Substring(122, 2);
                    string hexPitch = string.Concat(str[120], str[121], str[118], str[119]);// str.Substring(118, 2);
                    string hexVup = string.Concat(str[116], str[117], str[114], str[115]);// str.Substring(114, 2);
                    string hexVeast = string.Concat(str[112], str[113], str[110], str[111]);// str.Substring(110, 2);
                    string hexVnorth = string.Concat(str[108], str[109], str[106], str[107]);//str.Substring(106, 2);
                    string hexAltitude = string.Concat(str[100], str[101], str[98], str[99]);// str.Substring(98, 2); 


                    aeroscopedata.HomeLatitude = -1;
                    aeroscopedata.HomeLongitude = -1;
                    aeroscopedata.Id = -1;
                    aeroscopedata.SerialNumber = "";
                    aeroscopedata.Type = "";
                    aeroscopedata.UUID = "1";
                    aeroscopedata.UUIDLength = 1;
                    aeroscopetrajectory.Coordinates.Altitude = -1;
                    aeroscopetrajectory.Coordinates.Latitude = -1;
                    aeroscopetrajectory.Coordinates.Longitude = -1;
                    aeroscopetrajectory.SerialNumber = "";
                    aeroscopetrajectory.V_east = -1;
                    aeroscopetrajectory.V_north = -1;
                    aeroscopetrajectory.V_up = -1;
                    aeroscopetrajectory.Yaw = -1;

                    string hexLenghtUUID = str.Substring(148, 2);
                    string hexUUID = str.Substring(150, int.Parse(hexLenghtUUID, System.Globalization.NumberStyles.HexNumber) * 2);//???правильно ли так считать uuid
                    aeroscopedata.HomeLatitude = (double)ulong.Parse(hexHomeLat, System.Globalization.NumberStyles.HexNumber) / 174533;
                    aeroscopedata.HomeLongitude = (double)ulong.Parse(hexHomeLng, System.Globalization.NumberStyles.HexNumber) / 174533;
                    aeroscopedata.UUIDLength = (double)ulong.Parse(hexLenghtUUID, System.Globalization.NumberStyles.HexNumber);
                    aeroscopedata.Type = GetProductType(hexProductType);

                    aeroscopetrajectory.Time = DateTime.Now;
                    aeroscopetrajectory.V_north = (float)ulong.Parse(hexVnorth, System.Globalization.NumberStyles.HexNumber) / 100;
                    aeroscopetrajectory.V_east = (float)ulong.Parse(hexVeast, System.Globalization.NumberStyles.HexNumber) / 100;
                    aeroscopetrajectory.V_up = (float)ulong.Parse(hexVup, System.Globalization.NumberStyles.HexNumber) / 100;
                    aeroscopetrajectory.Yaw = int.Parse(hexYaw, System.Globalization.NumberStyles.HexNumber);
                    aeroscopetrajectory.Roll = int.Parse(hexRoll, System.Globalization.NumberStyles.HexNumber);
                    aeroscopetrajectory.Elevation = (float)ulong.Parse(hexHeight, System.Globalization.NumberStyles.HexNumber) / 10;//elevation==height??
                    aeroscopetrajectory.Coordinates.Altitude = int.Parse(hexAltitude, System.Globalization.NumberStyles.HexNumber);


                    //Random random1 = new Random();
                    //var drobnchastshir = (double)random1.Next(919071, 941509);

                    //double shirota = drobnchastshir / 1000000;
                    //shirota = shirota + 53;


                    //Random random2 = new Random();
                    //var drobnchastdolg = (double)random2.Next(627968, 641615);

                    //double dolgota = drobnchastdolg / 1000000;
                    //dolgota = dolgota + 27;


                    //aeroscopetrajectory.Coordinates.Latitude = shirota;
                    //aeroscopetrajectory.Coordinates.Longitude = dolgota;
                    aeroscopetrajectory.Coordinates.Latitude = (double)ulong.Parse(hexLat, System.Globalization.NumberStyles.HexNumber) / 174533;
                    aeroscopetrajectory.Coordinates.Longitude = (double)ulong.Parse(hexLng, System.Globalization.NumberStyles.HexNumber) / 174533;
                    aeroscopetrajectory.Pitch = int.Parse(hexPitch, System.Globalization.NumberStyles.HexNumber);
              

                    for (int i = 0; i < hexSerialNumber.Length; i += 2)
                        aeroscopedata.SerialNumber += Convert.ToChar(Convert.ToUInt32(hexSerialNumber.Substring(i, 2), 16));
                    aeroscopedata.UUID = "";//чтоюы не писало исключение null, в tcpclient, где проверяется совпадение поля длина и реальной длины 
                    for (int i = 0; i < hexUUID.Length; i += 2)
                        aeroscopedata.UUID += Convert.ToChar(Convert.ToUInt32(hexUUID.Substring(i, 2), 16));

                    return Tuple.Create(aeroscopedata, aeroscopetrajectory);
                }
            }
            catch (Exception ex)
            {
                aeroscopedata.SerialNumber = "error!";            
            }
            return null;
        }

        /// <summary>
        /// Parsing of data packets from Dji drones
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private Tuple<TableAeroscope, TableAeroscopeTrajectory> Parse2(string str)
        {
            TableAeroscope aeroscopedata = new TableAeroscope();
            TableAeroscopeTrajectory aeroscopetrajectory = new TableAeroscopeTrajectory();
            try
            {
                if (str != null)
                {
                    string serialNumber = string.Empty;
                    string uuid = string.Empty;
                    string hexSerialNumber = str.Substring(50, 32);

                    for (int i = hexSerialNumber.Length - 2; i >= 0; i -= 2)
                    {
                        if (hexSerialNumber[i] == '0' && hexSerialNumber[i + 1] == '0')
                            hexSerialNumber = hexSerialNumber.Remove(i);
                        else break;
                    }

                    string hexLng = string.Concat(str[88], str[89], str[86], str[87], str[84], str[85], str[82], str[83]);
                    string hexLat = string.Concat(str[96], str[97], str[94], str[95], str[92], str[93], str[90], str[91]);
                    string hexHomeLng = string.Concat(str[160], str[161], str[158], str[159], str[156], str[157], str[154], str[155]);
                    string hexHomeLat = string.Concat(str[168], str[169], str[166], str[167], str[164], str[165], str[162], str[163]);
                    //string hexPilotLng = string.Concat(str[152], str[153], str[150], str[151], str[148], str[149], str[146], str[147]);
                    //string hexPilotLat = string.Concat(str[144], str[145], str[142], str[143], str[140], str[141], str[138], str[139]);
                    string hexHeight = string.Concat(str[104], str[105], str[102], str[103]);// str.Substring(102, 2);
                    //string hexHeight = str.Substring(132, 4);   

                    string hexYaw = string.Concat(str[128], str[129], str[126], str[127]);
                    string hexRoll = string.Concat(str[124], str[125], str[122], str[123]);
                    string hexPitch = string.Concat(str[120], str[121], str[118], str[119]);
                    string hexVup = string.Concat(str[116], str[117], str[114], str[115]);/////разобраться здесь по байту!!!!
                    string hexVeast = string.Concat(str[112], str[113], str[110], str[111]);// str.Substring(110, 2);
                    string hexVnorth = string.Concat(str[108], str[109], str[106], str[107]);// str.Substring(106, 2);
                    string hexAltitude = string.Concat(str[100], str[101], str[98], str[99]);// str.Substring(98, 2);

                    string hexProductType = str.Substring(170, 2);//172??grey drone 
                    string hexLenghtUUID = str.Substring(172, 2);
                    string hexUUID = str.Substring(174, int.Parse(hexLenghtUUID, System.Globalization.NumberStyles.HexNumber) * 2);

                    aeroscopedata.HomeLatitude = -1;
                    aeroscopedata.HomeLongitude = -1;
                    aeroscopedata.Id = -1;
                    aeroscopedata.SerialNumber="";
                    aeroscopedata.Type = "";
                    aeroscopedata.UUID = "1";
                    aeroscopedata.UUIDLength = 1;
                    aeroscopetrajectory.Coordinates.Altitude = -1;
                    aeroscopetrajectory.Coordinates.Latitude = -1;
                    aeroscopetrajectory.Coordinates.Longitude = -1;
                    aeroscopetrajectory.SerialNumber = "";
                    aeroscopetrajectory.V_east = -1;
                    aeroscopetrajectory.V_north = -1;
                    aeroscopetrajectory.V_up = -1;
                    aeroscopetrajectory.Yaw = -1;

                    //Random random1 = new Random();
                    //var drobnchastshir = (double)random1.Next(919071, 941509);

                    //double shirota = drobnchastshir / 1000000;
                    //shirota = shirota + 53;


                    //Random random2 = new Random();
                    //var drobnchastdolg = (double)random2.Next(627968, 641615);

                    //double dolgota = drobnchastdolg / 1000000;
                    //dolgota = dolgota + 27;

                    //aeroscopetrajectory.Coordinates.Latitude = shirota;
                    //aeroscopetrajectory.Coordinates.Longitude = dolgota;
                    aeroscopetrajectory.Time = DateTime.Now;
                    aeroscopetrajectory.Coordinates.Latitude = (double)ulong.Parse(hexLat, System.Globalization.NumberStyles.HexNumber) / 174533;
                    aeroscopetrajectory.Coordinates.Longitude = (double)ulong.Parse(hexLng, System.Globalization.NumberStyles.HexNumber) / 174533;
                    aeroscopedata.HomeLatitude = (double)ulong.Parse(hexHomeLat, System.Globalization.NumberStyles.HexNumber) / 174533;
                    aeroscopedata.HomeLongitude = (double)ulong.Parse(hexHomeLng, System.Globalization.NumberStyles.HexNumber) / 174533;
                    aeroscopedata.Type = GetProductType(hexProductType);
                    aeroscopedata.UUIDLength = (double)ulong.Parse(hexLenghtUUID, System.Globalization.NumberStyles.HexNumber);
                    //pilotLongitude = (double)ulong.Parse(hexPilotLng, System.Globalization.NumberStyles.HexNumber) / 174533;//--what is it??
                    //pilotLatitude = (double)ulong.Parse(hexPilotLat, System.Globalization.NumberStyles.HexNumber) / 174533;//--what is it??
                    aeroscopetrajectory.Elevation = (float)ulong.Parse(hexHeight, System.Globalization.NumberStyles.HexNumber) / 10;
                    aeroscopetrajectory.V_north = (float)ulong.Parse(hexVnorth, System.Globalization.NumberStyles.HexNumber) / 100;
                    aeroscopetrajectory.V_east = (float)ulong.Parse(hexVeast, System.Globalization.NumberStyles.HexNumber) / 100;
                    aeroscopetrajectory.V_up = (float)ulong.Parse(hexVup, System.Globalization.NumberStyles.HexNumber) / 100;
                    aeroscopetrajectory.Yaw = (int)(int.Parse(hexYaw, System.Globalization.NumberStyles.HexNumber) / 57.296);
                    aeroscopetrajectory.Roll = (int)(int.Parse(hexRoll, System.Globalization.NumberStyles.HexNumber) / 57.296);
                    aeroscopetrajectory.Coordinates.Altitude = int.Parse(hexAltitude, System.Globalization.NumberStyles.HexNumber);
                    aeroscopetrajectory.Pitch = (int)(int.Parse(hexPitch, System.Globalization.NumberStyles.HexNumber) / 57.296);

                 

                    for (int i = 0; i < hexSerialNumber.Length; i += 2)
                        aeroscopedata.SerialNumber += Convert.ToChar(Convert.ToUInt32(hexSerialNumber.Substring(i, 2), 16));
                    aeroscopedata.UUID = "";//чтобы не писало исключение null, в tcpclient, где проверяется совпадение поля длина и реальной длины 
                    for (int i = 0; i < hexUUID.Length; i += 2)
                        aeroscopedata.UUID += Convert.ToChar(Convert.ToUInt32(hexUUID.Substring(i, 2), 16));

                    return Tuple.Create(aeroscopedata, aeroscopetrajectory);
                    ///placeAircraftOnMap(latitude, longitude, homeLatitude, homeLongitude, (int)height, yaw);
                }
            }
            catch (Exception ex)
            {
                aeroscopedata.SerialNumber = "error!";
                //return Tuple.Create(aeroscopedata, aeroscopetrajectory);
            }
            return null;
        }

        /// <summary>
        /// Returns product type 
        /// </summary>
        /// <param name="productType"></param>
        /// <returns></returns>
        private string GetProductType(string productType)
        {
            string emptystr = "";
            try
            {
                string ProductName = types.FirstOrDefault(x => x.Key.Equals(productType.ToLower() + "\t")).Value;
                return ProductName;
            }
            catch(Exception ex)
            {
               return emptystr;
            }
        }
    }
}






