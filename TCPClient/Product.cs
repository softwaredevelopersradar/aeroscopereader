﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TCPClient
{
    public class Product
    {
        public string ProductId
        {
            get;
            set;
        }
        public string ProductName
        {
            get;
            set;
        }
        public string CategoryName
        {
            get;
            set;
        }
    }
}
