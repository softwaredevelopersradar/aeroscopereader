﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Text;

namespace TCPClient
{
    public class MyEventArgsAeroScopeData: EventArgs
    {
        public TableAeroscope tableAeroscope;
        public TableAeroscopeTrajectory trajectory;
        public MyEventArgsAeroScopeData(TableAeroscope tableAeroscope, TableAeroscopeTrajectory trajectory)
        {
            this.tableAeroscope = tableAeroscope;
            this.trajectory = trajectory;
        }
    }
}
