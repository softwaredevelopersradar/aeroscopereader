﻿using System;
using System.IO;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.TypeResolvers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCPClient;
using UDPClient;
using KirasaModelsDBLib;
using ClientDataBase;
using System.Threading;
using InheritorsEventArgs;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Net;

using System.Text.Json;
using Newtonsoft.Json;

namespace AeroScopeReader
{
    public class Program
    {
        public static Yaml yaml = new Yaml();
        public static LocalProperties localProperties;
        public static ClientDB clientDB;
        public static TcpClient tcpClient = new TcpClient();
        public static UdpClient udpClient = new UdpClient();

        static bool exitSystem = false;

        

        #region Trap application termination
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            Console.WriteLine("Exiting system...");

            if (tcpClient != null) { tcpClient.Disconnect(); }
            if (udpClient != null) { udpClient.Disconnect(); }
            if (clientDB != null && clientDB.IsConnected()) { clientDB.Disconnect(); }

            Console.WriteLine("Cleanup complete");

            //allow main to run off
            exitSystem = true;

            //shutdown right away so there are no lingering threads
            Environment.Exit(-1);

            return true;
        }
        #endregion

        private static void Init()
        {
            try
            {
                localProperties = yaml.YamlLoad();
              
                tcpClient = new TcpClient();
                tcpClient.OnConnect += TcpConnected;
                tcpClient.OnDisconnect += TcpDisconnected;
                tcpClient.OnUpdateAeroscopeData += GetAeroscopeData;

                udpClient.OnConnectNet += UdpConnected;
                udpClient.OnDisconnectNet += UdpDisconnected;
                udpClient.Connect(localProperties.IPudpLocal, localProperties.UdpPortLocal, localProperties.IPudpRemote, localProperties.UdpPortRemote);

                clientDB = new ClientDB(localProperties.NameApplication, localProperties.endPoint);
                clientDB.OnConnect += DbConnected;
                clientDB.OnDisconnect += DbDisconnected;
                clientDB.OnUpData += DbUpData;

                (clientDB.Tables[NameTable.TableАeroscope] as ITableUpRecord<TableAeroscope>).OnDeleteRecord += OnDeleteTableAeroScopeRecord;
                (clientDB.Tables[NameTable.TableАeroscopeTrajectory] as ITableUpRecord<TableAeroscopeTrajectory>).OnDeleteRecord += OnDeleteTableTrajectoryRecord;

                clientDB.ConnectAsync();
                //while (!clientDB.IsConnected())
                //{
                //    clientDB.ConnectAsync();
                //    Thread.Sleep(1000);
                //    Console.WriteLine("Connection attempt to DB failed!");
                //}
                LoadDbData();

            }
            catch(Exception ex)
            {

            }
        }

        private static void ClientDB_OnUpData(object sender, DataEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loading of DB data
        /// </summary>
        public static Dictionary<string, int> DBAeroScopeDictionary = new Dictionary<string, int>();
        private static List<TableAeroscope> TableAeroScopeList = new List<TableAeroscope>();
        public static void LoadDbData()
        {
            try
            {
                TableAeroScopeList = LoadTableAeroscopeAsync().Result;//не грузит таблицу аэроскопа в данный лист с первого раза
            }
            catch(Exception ex)
            {

            }
            try 
            {
                TableAeroScopeList = LoadTableAeroscopeAsync().Result;//со второго раза грузит
                //LoadTableAeroscopeAsync().Result.ForEach(value => DBAeroScopeDictionary.Add(value.SerialNumber, 0));
                foreach (TableAeroscope value in TableAeroScopeList)
                {
                    DBAeroScopeDictionary.Add(value.SerialNumber, 0);
                }
                //LoadTableAeroscopeTrajectoryAsync().Result.ForEach(value => { return (DBAeroScopeDictionary[value.SerialNumber] <= value.Num) ? DBAeroScopeDictionary[value.SerialNumber] = value.Num : DBAeroScopeDictionary[value.SerialNumber]; });
                foreach (TableAeroscopeTrajectory value in LoadTableAeroscopeTrajectoryAsync().Result)
                {
                    if (DBAeroScopeDictionary[value.SerialNumber] <= value.Num)
                    {
                        DBAeroScopeDictionary[value.SerialNumber] = value.Num;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Informs about getting aeroscope data packet and sends data by UDP to the host
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static DateTime previouseTime = new DateTime();
        public static DateTime currentTime = new DateTime();
        public static TimeSpan packetInterval = new TimeSpan();
        public static int packetcounter = 0;
        public static double sum = 0;
        public static double averageTime = 0;
        public static System.Net.Sockets.UdpClient udpPacketSender = new System.Net.Sockets.UdpClient();
        public static IPEndPoint endPointClientUdp;
        public static void GetAeroscopeData(object sender, MyEventArgsAeroScopeData e)
        {
            try
            {
                currentTime = DateTime.UtcNow;
                packetcounter++;
                if (packetcounter > 1)
                {
                    packetInterval = currentTime.Subtract(previouseTime);
                    sum += packetInterval.Seconds + packetInterval.Milliseconds / 1000;
                }
                if (packetcounter == 16)
                {
                    averageTime = sum / 15;
                    Console.ResetColor();
                    Console.WriteLine("Average packet waiting time: " + averageTime + " sec");
                    packetcounter = 0;
                    sum = 0;
                }
                previouseTime = currentTime;
                Console.ForegroundColor = ConsoleColor.Green;
                if (String.IsNullOrWhiteSpace(localProperties.WritingSerialNumber) || String.IsNullOrEmpty(localProperties.WritingSerialNumber))//localProperties.WritingSerialNumber.Equals(null) || localProperties.WritingSerialNumber.Equals("") || localProperties.WritingSerialNumber.Equals(" "))
                {
                    Console.WriteLine("Status:" + " " + e.tableAeroscope.SerialNumber + " " + currentTime.ToString("HH:mm:ss:fff", CultureInfo.InvariantCulture) + " INTERVAL: " + packetInterval.Minutes + " min, " + packetInterval.Seconds + " sec, " + packetInterval.Milliseconds + " msec");
                }
                else if (localProperties.WritingSerialNumber.Length > 0 && localProperties.WritingSerialNumber.Equals(e.tableAeroscope.SerialNumber))
                {
                    Console.WriteLine("Status:" + " " + e.tableAeroscope.SerialNumber + " " + currentTime.ToString("HH:mm:ss:fff", CultureInfo.InvariantCulture) + " INTERVAL: " + packetInterval.Minutes + " min, " + packetInterval.Seconds + " sec, " + packetInterval.Milliseconds + " msec");
                }

                Console.ResetColor(); // сбрасываем в стандартный
            }
            catch (Exception ex)
            {

            }
            ///Sending AeroScope packet by UDP to lib user
            try
            {

                if (localProperties.IsSend2UDPASReceiver == true)
                {
                    ///client for getting AeroScope packets by udp               
                    udpPacketSender = new System.Net.Sockets.UdpClient();
                    endPointClientUdp = new IPEndPoint(IPAddress.Parse(localProperties.UDPASReceiverIp), localProperties.UDPASReceiverPort);
                    string json = JsonConvert.SerializeObject(e);
                    udpPacketSender.Send(Encoding.UTF8.GetBytes(json), Encoding.UTF8.GetBytes(json).Length, endPointClientUdp);
                    udpPacketSender.Close();
                    Console.WriteLine("UDP packet was sent to"+" IP: "+localProperties.UDPASReceiverIp+" Port: "+localProperties.UDPASReceiverPort);
                }

            }
            catch(Exception ex)
            {

            }
            try
            {
                if (clientDB != null && clientDB.IsConnected())
                {
                    WriteToDb(e.tableAeroscope, e.trajectory);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private static void TcpConnected(object sender, EventArgs e)
        {
            Console.WriteLine("Tcp Connected!");
        }
        private static void TcpDisconnected(object sender, EventArgs e)
        {
            Console.WriteLine("Tcp Disconnected!");
        }

        private static void UdpConnected(object sender, EventArgs e)
        {
            Console.WriteLine("Udp Connected!");
        }
        private static void UdpDisconnected(object sender, EventArgs e)
        {
            Console.WriteLine("Udp Disconnected!");
        }

        private static void DbConnected(object sender, ClientEventArgs e)
        {
            Console.WriteLine("Db Connected!");
        }
        private static void DbDisconnected(object sender, ClientEventArgs e)
        {
            Console.WriteLine("Db Disconnected!");
            while (!clientDB.IsConnected())
            {
                try
                {
                    clientDB.ConnectAsync();// = new ClientDB(localProperties.NameApplication, localProperties.endPoint);
                    Thread.Sleep(1000);
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Can not connect to DB!");
                }
            }
            LoadDbData();
        }
        /// <summary>
        /// event raised when aeroscope or trajectory db tables have been deleted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void DbUpData(object sender, DataEventArgs e)
        {
            if (e.AbstractData.ListRecords.Count==0)
            {
                switch (e.Name)
                {
                    case NameTable.TableАeroscope:
                        {
                            DBAeroScopeDictionary.Clear();
                            break;
                        }
                    case NameTable.TableАeroscopeTrajectory:
                        {
                            DBAeroScopeDictionary.Clear();
                            break;
                        }
                }
            }
        }
        /// <summary>
        /// raises when some AeroScope db table record have been deleted 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnDeleteTableAeroScopeRecord(object sender, TableAeroscope e)
        {
            DBAeroScopeDictionary.Remove(e.SerialNumber);
        }
        /// <summary>
        /// raises when some trajectory db table record have been deleted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnDeleteTableTrajectoryRecord(object sender, TableAeroscopeTrajectory e)
        {
            DBAeroScopeDictionary[e.SerialNumber] = DBAeroScopeDictionary[e.SerialNumber]-1;
        }
        
        /// <summary>
        /// returns data from TableAeroscope 
        /// </summary>
        /// <returns></returns>
        public static async Task<List<TableAeroscope>> LoadTableAeroscopeAsync()
        {
            return await clientDB.Tables[NameTable.TableАeroscope].LoadAsync<TableAeroscope>();//ошибка здесь если просто load, loadasync не хочет(Message = "Error: No Connection! ")
        }
        public static async Task<List<TableAeroscopeTrajectory>> LoadTableAeroscopeTrajectoryAsync()
        {
            return await clientDB.Tables[NameTable.TableАeroscopeTrajectory].LoadAsync<TableAeroscopeTrajectory>();//ошибка здесь если просто load, loadasync не хочет(Message = "Error: No Connection! ")
        }
        /// <summary>
        /// Writes aeroscope packet data to data base with dictionary
        /// </summary>
        /// <param name="aeroscopedata"></param>
        /// <param name="aeroscopeTrajectory"></param>
        public static async void WriteToDb(TableAeroscope aeroscopedata, TableAeroscopeTrajectory aeroscopeTrajectory)
        {
            try
            {               
                //запись в БД
                if (DBAeroScopeDictionary.ContainsKey(aeroscopedata.SerialNumber)) 
                {
                    await clientDB.Tables[NameTable.TableАeroscope].ChangeAsync(aeroscopedata);                   
                    DBAeroScopeDictionary[aeroscopedata.SerialNumber] += 1; //NUM of new aeroscope trajectory data have increased by 1
                    aeroscopeTrajectory.Num = (short)DBAeroScopeDictionary[aeroscopedata.SerialNumber];
                }               
                else
                {
                    await clientDB.Tables[NameTable.TableАeroscope].AddAsync(aeroscopedata);
                    DBAeroScopeDictionary.Add(aeroscopedata.SerialNumber, 0);
                    aeroscopeTrajectory.Num = 0;
                }
                aeroscopeTrajectory.SerialNumber = aeroscopedata.SerialNumber;
                await clientDB.Tables[NameTable.TableАeroscopeTrajectory].AddAsync(aeroscopeTrajectory);
               
            }
            catch(Exception ex)///иногда ошибки здесь. Поток находится в прерывании 
            {

            }           
        }

        static void Main(string[] args)
        {
            bool versionOLD = false;//waiting and reading aeroscope packets in thread with time limit(450msec) if true. waiting and reading aeroscope packets in while(_continue) cycle if false

            if (versionOLD)
            {
               M1();//creating thread thrRead for waiting and reading aeroscope packets 
            }
            else
            {
               M2(); //waiting of read cycle is deleted. streamclient.read() method is waiting aeroscope pocket in while(_continue) cycle that ends after aeroscope himself disconnects connection(some time after connection)
            }
        }

        /// <summary>
        /// Waiting and reading aeroscope answers in thread thrRead
        /// </summary>
        static private void M1()
        {
            // Some boilerplate to react to close window event, CTRL-C, kill, etc
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);
            Init();
            while (true)
            {
                try
                {
                    // code goes here 
                    udpClient.SendQueryFirst();
                    udpClient.SendQuerySecond();
                    tcpClient._continue = true;
                    tcpClient.Connect(localProperties.IPtcpClient, localProperties.TcpPortClient, localProperties.IPtcpServer, localProperties.TcpPortServer);
                    Console.ForegroundColor = ConsoleColor.Red; // устанавливаем цвет
                    if (localProperties.IsWritingQuery == true)
                    {
                        Console.WriteLine("QUERY to AeroScope!    " + DateTime.UtcNow.ToString("HH:mm:ss:fff", CultureInfo.InvariantCulture));
                    }
                    Console.ResetColor(); // сбрасываем в стандартный
                    Thread.Sleep(450);
                    tcpClient._continue = false;

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                    tcpClient.Disconnect();
                    udpClient.Disconnect();
                    clientDB.Disconnect();
                }


            }
        }

        /// <summary>
        /// Waiting and reading aeroscope answers without thread just in while(_continue) cycle. More effective in cpu usage way
        /// </summary>
        static private void M2()
        {
            // Some boilerplate to react to close window event, CTRL-C, kill, etc
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);
            Init();
            while (true)
            {
                try
                {
                    // code goes here 
                    udpClient.SendQueryFirst();
                    udpClient.SendQuerySecond();
                    //tcpClient._continue = true;
                    tcpClient.ConnectNoReadThr(localProperties.IPtcpClient, localProperties.TcpPortClient, localProperties.IPtcpServer, localProperties.TcpPortServer);
                    Console.ForegroundColor = ConsoleColor.Red; // устанавливаем цвет
                    //if (localProperties.IsWritingQuery == true)
                    //{
                    //    Console.WriteLine("QUERY to AeroScope!    " + DateTime.UtcNow.ToString("HH:mm:ss:fff", CultureInfo.InvariantCulture));
                    //}
                    Console.ResetColor(); // сбрасываем в стандартный
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                    tcpClient.Disconnect();
                    udpClient.Disconnect();
                    clientDB.Disconnect();
                }
            }
        }
    }
}
