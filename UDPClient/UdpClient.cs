﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace UDPClient
{
    public class UdpClient 
    {
        
        private const byte LEN_CMD = 9;
        private const byte MAX_SIZE = 255;

        private System.Net.Sockets.UdpClient udpClient;              // udp client
        private IPEndPoint ipEndPointLocal;       // local point
        private IPEndPoint ipEndPointRemote;      // remote point
        private Thread thrRead;                   // thread for reading*/

        string strIPRemote = "";

        public event EventHandler OnConnectNet;
        public event EventHandler OnDisconnectNet;


        public event EventHandler OnReadByte;
        public event EventHandler OnWriteByte;


        public UdpClient()
        {

        }
        // Create EndPoint
        public void Connect(string strIPClientLocal, int iPortLocal, string strIPClientRemote, int iPortRemote)
        {
            if (udpClient != null)
            {
                udpClient.Close();
                udpClient = null;
            }

            if (thrRead != null)
            {
                thrRead.Abort();
                thrRead.Join(10);
                thrRead = null;
            }

            if (ipEndPointRemote != null)
                ipEndPointRemote = null;

            if (ipEndPointLocal != null)
                ipEndPointLocal = null;

            try
            {
                ipEndPointLocal = new IPEndPoint(IPAddress.Parse(strIPClientLocal), iPortLocal);
                ipEndPointRemote = new IPEndPoint(IPAddress.Parse(strIPClientRemote), iPortRemote);

                strIPRemote = strIPClientRemote;

                // if simulator
                if (strIPRemote == "127.0.0.1")
                    udpClient = new System.Net.Sockets.UdpClient(iPortRemote);
                else
                    udpClient = new System.Net.Sockets.UdpClient(ipEndPointLocal);

                thrRead = new Thread(new ThreadStart(ReadData)); 
                thrRead.IsBackground = true;
                thrRead.Start();

                OnConnectNet?.Invoke(this, null);
            }
            catch (System.Exception ex)
            {
                OnDisconnectNet?.Invoke(this, null);
            }
        }

        // Destroy EndPoint
        public void Disconnect()
        {
            if (udpClient != null)
            {
                udpClient.Close();
                udpClient = null;
            }

            if (thrRead != null)
            {
                thrRead.Abort();
                thrRead.Join(10);
                thrRead = null;
            }

            if (ipEndPointRemote != null)
                ipEndPointRemote = null;

            if (ipEndPointLocal != null)
                ipEndPointLocal = null;

            if (OnDisconnectNet != null)
                OnDisconnectNet?.Invoke(this, null);
        }

        
        // Write data (byte) 
        private bool WriteData(byte[] bData)
        {
            try
            {

                udpClient.Send(bData, bData.Length, ipEndPointRemote);

                OnWriteByte?.Invoke(bData, null);

                return true;
            }
            catch (System.Exception ex)
            {

                return false;
            }
        }

      
        // Read data (byte) (thread)
        public void ReadData()
        {
            // buffer for reading bytes
            byte[] bBufRead;
            bBufRead = new byte[MAX_SIZE];
            byte[] bBufSave = null;

            // length of reading bytes
            int iReadByte = -1;

            int iTempLength = 0;

            while (true)
            {
                try
                {
                    // if simulator
                    if (strIPRemote == "127.0.0.1")
                    {
                        IPEndPoint ipendpoint = null;

                        bBufRead = udpClient.Receive(ref ipendpoint);
                    }
                    else
                    {
                        bBufRead = udpClient.Receive(ref ipEndPointLocal);

                    }

                    if (ipEndPointLocal.Address.ToString() == strIPRemote)
                    {
                        iReadByte = bBufRead.Length;

                        if (iReadByte == 0)
                        {
                            if (OnDisconnectNet != null)
                                OnDisconnectNet?.Invoke(this, null);
                        }

                        if (iReadByte > 0)
                        {
                            Array.Resize(ref bBufRead, iReadByte);

                            // raise event 
                            if (OnReadByte != null)
                                OnReadByte?.Invoke(bBufRead, null);

                            if (bBufSave == null)
                                bBufSave = new byte[iReadByte];
                            else
                                Array.Resize(ref bBufSave, bBufSave.Length + iReadByte);

                            // copy buffer for read to buffer for save
                            Array.Copy(bBufRead, 0, bBufSave, iTempLength, iReadByte);

                            // set current value of saving bytes
                            iTempLength += iReadByte;

                            
                        }
                    }
                }

                catch (System.Exception)
                {

                    Disconnect();

                    return;
                }
            }
        }

        public bool SendQueryFirst()
        {
            byte[] bytes1 = new byte[] { 0x55, 0x1d, 0x04, 0xdf, 0x0a, 0x0d, 0x38, 0x35, 0x40, 0x09, 0xc0, 0x04, 0x0e, 0x30, 0x51, 0x52, 0x44, 0x47, 0x35, 0x35, 0x30, 0x30, 0x33, 0x30, 0x30, 0x37, 0x38, 0x12, 0xf3 };
            return(WriteData(bytes1));

        }

        public bool SendQuerySecond()
        {
            byte[] bytes2 = new byte[] { 0x55, 0x38, 0x04, 0xe1, 0x0a, 0x0d, 0x39, 0x35, 0x40, 0x09, 0xc0, 0x02, 0x0e, 0x30, 0x51, 0x52, 0x44, 0x47, 0x35, 0x35, 0x30, 0x30, 0x33, 0x30, 0x30, 0x37, 0x38, 0x0c, 0x31, 0x39, 0x32, 0x2e, 0x31, 0x36, 0x38, 0x2e, 0x31, 0x2e, 0x31, 0x30, 0x0d, 0x32, 0x35, 0x35, 0x2e, 0x32, 0x35, 0x35, 0x2e, 0x32, 0x35, 0x35, 0x2e, 0x30, 0xb3, 0xc5 };
            return (WriteData(bytes2));

        }
        public bool SendQueryThird()
        {
            byte[] bytes3 = new byte[] { 0x55, 0x1d, 0x04, 0xdf, 0x0a, 0x0d, 0x3c, 0x35, 0x40, 0x09, 0xc0, 0x04, 0x0e, 0x30, 0x51, 0x52, 0x44, 0x47, 0x35, 0x35, 0x30, 0x30, 0x33, 0x30, 0x30, 0x37, 0x38, 0xa2, 0x2e};
            return (WriteData(bytes3));

        }

        public bool SendQueryFourth()
        {
            byte[] bytes4 = new byte[] { 0x55, 0x0e, 0x04, 0x66, 0x0a, 0x0d, 0x3d, 0x35, 0x40, 0x09, 0xc0, 0x01, 0x18, 0x08};
            return (WriteData(bytes4));

        }

    }
}
