﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace WpfApp1 
{
    public partial class ConnectDB
    {
        // ИРИ БПЛА
        public List<TableUAVRes> lUAVRes = new List<TableUAVRes>();
        // Траектория ИРИ БПЛА
        public List<TableUAVTrajectory> lUAVTrajectory = new List<TableUAVTrajectory>();
        // Локальные пункты приема (ЛПП)
        public List<TableReceivingPoint> lLocalPoints = new List<TableReceivingPoint>();
        // Удаленные пункты приема(УПП)
        public List<TableReceivingPoint> lRemotePoints = new List<TableReceivingPoint>();
        // Известные частоты (ИЧ)
        public List<TableFreqRanges> lFreqKnown = new List<TableFreqRanges>();
        // Диапазоны радиоразведки (ДРР)
        public List<TableFreqRanges> lFreqRangesRecon = new List<TableFreqRanges>();
        // Аэроскоп БПЛА
        public List<TableAeroscope> lAeroscope = new List<TableAeroscope>();
        // Траектория А БПЛА
        public List<TableAeroscopeTrajectory> lATrajectory = new List<TableAeroscopeTrajectory>();

        private int selectedUAVRes = -2;
        private int selectedAeroscope = -2;
    }
}
